# bookmarkmenu

# Overview
Bookmark storage using the menu back end.

# Usage
```
Usage:
  bookmarkmenu -h, --help                       Display this message.
  bookmarkmenu -i, --init                       Initialize a new store.
  bookmarkmenu -a, --add [item-name] [url] [tags...]
                                                Add an item to the store.
  bookmarkmenu -r, --remove [item-name]         Remove an item from the store.
  bookmarkmenu -l, --list                       List all items in the store.
  bookmarkmenu -o, --output [item-name]         Output the parsed version of an item in the store.
  bookmarkmenu -e, --edit [item-name]           Edit an item in the store.
  bookmarkmenu -d, --dmenu                      Select and open an item in the store with dmenu.
```

# Downloads
Currently, there are no official release of `bookmarkmenu`.

* You can download and install it from the AUR if you are running an arch system via [menu-bookmark-git](https://aur.archlinux.org/packages/menu-bookmark-git/).
* The only other way to install menu is to download the source located at [GitLab](https://gitlab.com/KNIX3/bookmarkmenu/).

# Dependencies
* A POSIX shell
* [menu](https://gitlab.com/KNIX3/menu)
* [dmenu](https://tools.suckless.org/dmenu)
